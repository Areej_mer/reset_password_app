<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class ResetPasswordController extends Controller
{
    public function resetPassword()
    {
        request()->validate([
            'mobile' => 'required|exists:users',
        ]);

        $user = User::where('mobile', request('mobile'))->first();
        $otp_code = rand(100000, 999999);
        $user->code = $otp_code;
        Log::info("otp code = " . $otp_code);
        $user->save();

        return response()->json(['message' => 'success', 'data' => 'code generated successfully']);
    }

    public function resetPasswordConfirm(Request $request)
    {
        $request->validate([
            'mobile' => 'required|exists:users',
            'new_password' => 'required|min:6',
            'code' => 'required'
        ]);

        $user = User::where('mobile', request('mobile'))->first();

        if ($user->code != $request->code) {
            return response()->json(['message' => 'Invalid Code']);
        }

        $user->code = null;
        $user->password = Hash::make($request->new_password);
        $user->save();

        return response()->json(['message' => "success", 'data' => $user]);
    }

}
