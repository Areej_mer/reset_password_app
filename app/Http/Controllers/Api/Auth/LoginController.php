<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login()
    {
        request()->validate([
            'mobile' => 'required|exists:users',
            'password' => 'required|min:6',
        ]);

        $data = request()->only('mobile', 'password');

        if (auth()->attempt($data)) {
            $user = User::where('id', auth()->id())->first();
            if (!$user->token) {
                $user->token = $user->createToken('API')->plainTextToken;
                $user->save();
            }
            return response()->json(['message' => 'Success', "data" => $user, 'token' => $user->token]);
        }
        return response()->json(['message' => 'Username or Password is Incorrect']);
    }
}
