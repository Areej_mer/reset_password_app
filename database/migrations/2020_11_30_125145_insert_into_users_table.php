<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class InsertIntoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert(
            array(
                'name' => 'user1',
                'email' => 'user1@domain.com',
                'mobile' => '0912345678',
                'password' => Hash::make('password'),
            )
        );
        DB::table('users')->insert(
            array(
                'name' => 'name2',
                'email' => 'user2@email.com',
                'mobile' => '0987654321',
                'password' => Hash::make('password'),
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
