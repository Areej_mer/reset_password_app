<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function() {

    Route::post('login', 'Api\Auth\LoginController@login');
    Route::post('reset/password', 'Api\Auth\ResetPasswordController@resetPassword');
    Route::post('reset/password/confirm', 'Api\Auth\ResetPasswordController@resetPasswordConfirm');

});

